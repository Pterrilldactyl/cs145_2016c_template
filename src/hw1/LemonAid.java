package hw1;

import java.util.Scanner;

public class LemonAid {
	public static void main(String[] args) {
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		
		
		
		
		System.out.print("How many parts lemon juice? ");
		int partsLemon = in.nextInt();
		System.out.print("How many parts sugar? ");
		int partsSugar = in.nextInt();
		System.out.print("How many parts water? ");
		int partsWater = in.nextInt();
		System.out.print("How many cups of lemonade? ");
		int cupsOfLemonade = in.nextInt();
				
		//nCupsLemonade * nPartsOfIndividualIngredient / nTotalParts
		
		double totalParts = partsLemon + partsSugar + partsWater;
		double partMultiplier = cupsOfLemonade/totalParts;
		double cupsOfLemon = partsLemon*partMultiplier;
		double cupsOfSugar = partsSugar*partMultiplier;
		double cupsOfWater = cupsOfLemonade * partsWater / totalParts;
		System.out.println("Amounts (in cups):");
		System.out.println("  Lemon juice: " + cupsOfLemon);
		System.out.println("  Sugar: " + cupsOfSugar);
		System.out.println("  Water: " + cupsOfWater);
	}

}
