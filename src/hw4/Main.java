package hw4;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;

public class Main {
	public static void main(String[] args) throws IOException {
		int width = 256;
		int height = width;
		BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		JFileChooser dialog = new JFileChooser();
		int status = dialog.showOpenDialog(null);
		if (status == JFileChooser.APPROVE_OPTION)
			try {
				{
					File file = dialog.getSelectedFile();
					BufferedImage src = ImageIO.read(file);
					BufferedImage dst = ImageUtilities.swapCorners(src);
					ImageIO.write(dst, FilenameUtilities.getExtension(file),
							FilenameUtilities.appendToName(file, "swapped"));
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}

}
